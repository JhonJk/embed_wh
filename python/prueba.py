# -*- coding:utf-8 -*-
import gi
gi.require_version('WebKit2','4.0')
gi.require_version('Gtk','3.0')

from gi.repository import Gtk
from gi.repository import WebKit2 as WebKit

def webkit_inspector(bwh):
    bwh.show

window = Gtk.Window()
## Here's the new part.. We created a global object called 'browserholder' which will contain the WebKit rendering engine, and we set it to 'WebKit.WebView()' which is the default thing to do if you want to add a WebKit engine to your program.

browserholder = WebKit.WebView()

## The default URL to be loaded, we used the 'load_uri()' method.
browserholder.load_uri("https://web.whatsapp.com")

bwh = WebKit.WebInspector()

browserholder.connect('inspect-web-view', webkit_inspector(bwh))
## We used the '.add()' method to add the 'browserholder' object to the scrolled window, which contains our WebKit browser.
window.add(browserholder)


## And finally, we showed the 'browserholder' object using the '.show()' method.
browserholder.show()

# bhw = bwh.inspected-uri()
# print (bhw)

# dev_tools.show()

## Give that developer a cookie !
window.show_all()
Gtk.main()
