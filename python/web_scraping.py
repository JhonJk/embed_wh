from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import sys
import pyautogui
import datetime
import re
from time import *

non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
st=time()

def eta(seconds):
    sec=seconds-st
    return "ETA: "+str(datetime.timedelta(seconds=sec))
def valid_date(datestring):

        try:
                mat=re.match('(\d{2})[/.-](\d{2})[/.-](\d{4})$', datestring)
                if mat is not None:
                        datetime.datetime(*(map(int, mat.groups()[-1::-1])))
                        return True
        except ValueError:
                pass
        return False

def findmsg(name):
    sleep(2);partial=0
    try:
        driver.find_element_by_class_name('_1ays2').click()
    except:
        return None
    sleep(2)
    for i in range(40):
        sleep(1)
        pyautogui.press('up')
    msg=[name];prev='Unknown'
    sleep(8)
    htmlcode=(driver.page_source).encode('utf-8')
    soup = BeautifulSoup(htmlcode,features="html.parser")
    cnt=0
    for tag in soup.find_all('span'):
        classid=tag.get('class')
        if classid==['_F7Vk', 'selectable-text', 'invisible-space', 'copyable-text']:
            msg.append([tag.text.translate(non_bmp_map).replace('\n', '')])
        if classid==['_3fnHB']:
            try:
                if msg[-1][-1] in [1,2]:
                    msg[-1].append(tag.text)
            except:
                partial=1
        if classid in [['EopGb', '_3HIqo'],['EopGb']]:
            try:
                msg[-1].append(len(classid))
            except:
                partial=1
        if classid == ['_F7Vk']:
            try:
                if tag.text in ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY', 'TODAY', 'YESTERDAY'] or valid_date(tag.text):
                    msg[-1].append(tag.text)
            except:
                if tag.text in ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY', 'TODAY', 'YESTERDAY'] or valid_date(tag.text):
                    prev=tag.text
                partial=1
    for i in msg:
        if len(i)>4:
            i=i[:4]
    
    for i in msg[1:]:
        if len(i)==3:
            i.append(prev)
        else:
            prev=i[-1]
    chats.append(msg)

class Browser(webdriver.ChromeOptions):

    def __init__(self, *args, **kwargs):
        super(Browser, self).__init__(*args, **kwargs)
        self.driver = webdriver.Remote(
            command_executor='http://190.60.4.103:4449/wd/hub',
            options=self
        )
        # self.driver = webdriver.Chrome()
        self.driver.get("https://web.whatsapp.com/")
        chats=[]
        print("Chrome has been automated",eta(time()))
        sleep(2)
    
    def scroll(self):
        self.action = webdriver.ActionChains(self.driver)
        self.action.send_keys(Keys.PAGE_DOWN).perform()
        self.action.send_keys(Keys.DOWN).perform()
        sleep(0.5)
        print("scroll")

    def authentication(self):        
        qr_code = self.driver.find_elements_by_tag_name('canvas')
        qr_code[0].screenshot('static/qr_code.png')

    def contacts_retrieve(self):
        
        sleep(10)
        self.new_chat = self.driver.find_elements_by_xpath('//div[@title="Nuevo chat"]')

        if self.new_chat:    
            self.new_chat[0].click()
            print ('New chat Click')
            sleep(1)
            self.action = webdriver.ActionChains(self.driver)
            self.action.send_keys(Keys.TAB).perform()
            
            mycon=set()
            while(True):
                self.scroll()
                contacts = self.driver.find_elements_by_class_name('_3Tw1q')
                newcon=set([j.text for j in contacts])
                if len(newcon|mycon)==len(mycon):
                    break
                else:
                    mycon=newcon|mycon

            contact=sorted(list(mycon),key=str.casefold)
            print(contact, len(contact))
            return contact

        else:
            sleep(1)
            return False
                
            
            



# driver = webdriver.Chrome()
# driver.get("https://web.whatsapp.com/")
# sleep(13)
# chats=[]
# print("Chrome has been automated",eta(time()))

# elem = driver.find_elements_by_class_name('chGSa')
# print(elem)
# elem[0].click()

# new_chat = driver.find_elements_by_xpath('//div[@title="Nuevo chat"]') # return a list
# if new_chat:
#     print(new_chat)
#     sleep(10)
#     # new_chat[0].click()
#     print("Web Whatsapp Authetication success",eta(time()))

# mycon=set()
# while(True):
#     scroll()
#     contacts = driver.find_elements_by_class_name('_3Tw1q')

#     newcon=set([j.text for j in contacts])
#     if len(newcon|mycon)==len(mycon):
#         break
#     else:
#         mycon=newcon|mycon

# contact=sorted(list(mycon),key=str.casefold)
# print(contact)
# print(len(contact),"conversations has been retrieved",eta(time()))

# rotate = dict()

# match=dict()
# for i in contact:
#     match[i.lower()]=i
# contacts=[i.lower() for i in contact]
# for i in range(len(contacts)):
#     cnt=0
#     for j in range(i):
#         if contacts[i] in contacts[j]:
#             cnt+=1
#     rotate[match[contacts[i]]]=cnt
# sleep(2)
# scrap=0
# for i in contact:
#     driver.find_element_by_xpath('//div[@title="Nuevo chat"]').click()
#     sleep(2)
#     driver.find_element_by_class_name('_2Evw0').click()
#     pyautogui.typewrite(i)
#     sleep(2)
#     for j in range(rotate[i]+1):
#         pyautogui.press('down')
#     pyautogui.press('enter')
#     findmsg(i)
#     try:
#         driver.find_element_by_class_name('_2Evw0').click()
#     except:
#         pyautogui.press('esc')
#         driver.find_element_by_class_name('_2Evw0').click()
#     scrap+=1
#     if scrap==1:
#         print('['+i,"Success",eta(time()),end='')
#     elif scrap==len(contact):
#         print(','+i,"Success"+eta(time())+']')
#     else:
#         print(','+i,"Success",eta(time()),end='')

# print("Messages has been successfully retrieved",eta(time()))
def main():
    browser = Browser()
    sleep(5)
    browser.authentication()

if __name__=='__main__':
    main()
