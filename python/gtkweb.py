import gi
gi.require_version('WebKit2','4.0')
gi.require_version('Gtk','3.0')

from gi.repository import Gtk, Gio, Gdk
from gi.repository import WebKit2 as Wk

import threading

class Browser(Gtk.Window):

    def __init__(self, *args, **kwargs):
        super(Browser, self).__init__(*args,**kwargs)

        self.wc = Wk.WebContext()
        self.set_size_request(800,300)

        self.browserholder = Wk.WebView.new_with_context(self.wc)     
        self.browserholder.load_uri("https://web.whatsapp.com")

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add(self.browserholder)
        self.add(scrolled_window)
        scrolled_window.show_all()

        self.show()
        
class Inspector(Gtk.Window):
    def __init__(self, view, *args, **kwargs):
        super(Inspector, self).__init__(*args, **kwargs)
        self.set_size_request(1200,700)
        settings = Wk.Settings.new()   
        settings.set_property('enable-developer-extras', True)
        view.set_settings(settings)

        self.inspector = view.get_inspector()

        self.scrolled_window = Gtk.ScrolledWindow()
        self.add(self.scrolled_window)
        self.scrolled_window.show()

        self.inspector.connect("open-window", self.inspect, view)

        self.webview = Wk.WebView()
        self.scrolled_window.add(self.webview)

    def inspect(self, *args, **kwargs):
        file_ = browser.browserholder.save_to_file(Gio.File.new_for_path("page"),0,None,None,None)
        print('Hello')
        self.scrolled_window.show_all()
        self.show()
        return self.webview

class MiHilo(threading.Thread):

    def run(self):
        browser = Browser()
        browser.connect("destroy", Gtk.main_quit)
        inspector = Inspector(browser.browserholder)
        Gtk.main()
    
def main():
    hilo = MiHilo()
    hilo.start()
    

if __name__ == "__main__":
    main()
