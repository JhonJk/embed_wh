from django.db import models

class Contacts(models.Model):
    contact=models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.contact