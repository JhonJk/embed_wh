from django.views.generic import TemplateView, ListView
from django.shortcuts import render
from django.http import HttpResponse
from django.core.signals import request_started, request_finished
from django.dispatch import receiver

from wh_import.models import Contacts
from python.web_scraping import Browser    

class HomePage(TemplateView):
    template_name = 'index.html'

class ContactsList(ListView):
    model = Contacts

def return_request(request):
    global browser
    browser = Browser()
    browser.authentication()
    return render(request, 'prueba.html')


@receiver(request_finished)
def callback(sender, **kwargs):
    print(sender.__name__)
    if sender.__name__ == 'StaticFilesHandler':
        print("Inside")
        contacts = browser.contacts_retrieve()
        if contacts:
            for contact in contacts:
                Contacts.objects.create(contact = contact)
            print(contacts)



    
